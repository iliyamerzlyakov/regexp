package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class DoubleRegExp {
    public static void main(String[] args) {
        String string = " 2.5 -5.78 плюс + +67 .8 9. +. 23.12e+10 ";
        Pattern pattern = Pattern.compile("(\\d+\\.\\d*)([eEеЕ][+-]\\d+)");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
}